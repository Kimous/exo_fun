import requests
import json

api_url = "https://opendata.bordeaux-metropole.fr/api/records/1.0/search/?dataset=ci_courb_a&rows=193"

response = requests.get(api_url)

Data = response.json() 

# dump in file
with open('data.json', 'w') as f:
    json.dump(Data, f)


# Create html file
Func = open("index.html","w") 
   
# Add base html
Func.write("<html>\n<head>\n<title> Traffic de Bordeaux </title>\n</head> \n <body> \n <h1>Prevision traffic <u>Bordeaux</u></h1> \n <table border=1px > \n <tr> <th>heure</th> <th>prevision</th> </tr> \n")

# Close html file 
Func.close()

# read/load json file
increment = 0
with open('data.json') as user_file:
  parsed_json = json.load(user_file)
  for i in parsed_json['records']:
    # open html file
    Func = open("index.html","a") 
    Func.write("\n <tr>")
    Func.write("<td>")
    # Close html file 
    Func.close()
    print(parsed_json['records'][increment]['fields']['bm_heure'], file=open('index.html', 'a'))
    # open html file
    Func = open("index.html","a")   
    Func.write("</td> \n <td>")
    # Close html file 
    Func.close()
    print(parsed_json['records'][increment]['fields']['bm_prevision'], file=open('index.html', 'a'))
    # open html file
    Func = open("index.html","a") 
    Func.write("</td>")
    Func.write("</tr>")
    increment += 1


# open html file
Func = open("index.html","a") 

# End of html file
Func.write("\n </table> \n</body></html>") 

# Close html file 
Func.close()

